# Server Helpers
**This repo is archived as we do not use lighttpd anymore (and LetsEncrypt is automated now)**
Only kept for "historical reasons":-)

---

This repository contains tiny scripts for simple system administration tasks where docker is not an option. 

The scripts are considered work in progress and will be improved. This repository is about to clean up and combine three dirty versions that existed in the past on different instances ...

# Scripts included
## lighty
lighty (short forlighttpd) is a cli utility for simple lighttpd site administration.
For every larger server software there is a complex web management interface whereas there 
is no such tool for lighttpd - or at least there was nothing that suited my needs. 

Thus I wrote some scripts to do all the work for me. After years of usage I decided to 
put all these different versions together and started on cleaning things up. 

### Requirements
Needs at least some config files provided with Debian GNU/Linux and certbot. Assuming lighttpd config is at /etc/lighttpd

### Usage:
Execute the script by invoking `lighty`. You might symlink the script into some of your local PATH folders (for me `~/bin`). The first, second arguments are switched with case like this:
- `init` copies the provided configuration files into lighttpd assuming this is `/etc/lighttpd`. Requires configuration files provided with Debian GNU/Linux.
- `new` let's you create a new site. It's interactive and asks for template files (e. g. host, proxy), the domain name (it obtains a Let's Encrypt cert for you) and a destination (a third template parameter, e. g. for a proxy port)
- `site` requires a second parameter
  - `enable` $3 generates a lighttpd config file from site config located at `/etc/lighttpd/$3.site` and puts it into `/etc/lighttpd/conf-enabled/50-$3.conf`
  - `disable` simply removes `/etc/lighttpd/conf-enabled/50-$3.conf` assuming you can always regenerate it.
- `test` just tests you lighttpd config file using `/usr/sbin/lighttpd -t -f /etc/lighttpd/lighttpd.conf` (you can conider changing this to `-tt`)
- `restart` restarts lighttpd using the `service` command.
- `upgrade` runs the necessary steps in order to upgrade lighttpd to a newer version 
if you are in a lighttpd source folder. `git clone` it if you need.



